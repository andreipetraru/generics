package gen;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class Capture {
	static <T> void reverse0(List<T> list) {
		List<T> tmp = new ArrayList<>(list);
		for (int i = 0; i < list.size(); i++) {
			list.set(i, tmp.get(list.size() - i - 1));
		}
	}

	static void reverse(List<?> list) {
		reverse0(list);
	}

	@Test
	public void capture() {
		List<Integer> ints = new ArrayList<>();
		ints.add(1);
		ints.add(2);
		ints.add(3);
		reverse(ints);
	}

}
