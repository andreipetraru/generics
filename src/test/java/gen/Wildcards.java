package gen;


import model.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Wildcards {
	public static void main(String[] args) {
		List<Game> games = Games.GENERIC_GAMES;
		List<BoardGame> boardGames = Games.BOARD_GAMES;
		List<VideoGame> videoGames = Games.VIDEO_GAMES;
		List<FirstPersonShooter> firstPersonShooters = Games.FIRST_PERSON_SHOOTERS;
		List<PartyGame> partyGames = Games.PARTY_GAMES;
		List<MoreGeneric<BoardGame>> generics = new ArrayList<>();
	}

	public static <T extends Comparable<? super T>> T max(List<? extends T> list) {
		return Collections.max(list);
	}

	public static void add(FirstPersonShooter game, List<FirstPersonShooter> games) {
		games.add(game);
	}

	public static long countDistinct(List<Game> games) {
		return games.stream()
				.distinct()
				.count();
	}

	static List<? super Integer> bestList() {
		return List.of(1, 2, 3);
	}
}

class Generic implements Comparable<Generic> {
	@Override
	public int compareTo(Generic o) {
		return 0;
	}
}

class MoreGeneric<T> extends Generic {
	T t;
}
