package gen;

import model.Game;
import model.Games;
import model.VideoGame;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class Variance {
	@Test
	public void objects() {
		Game game = Games.SOTRON;
		VideoGame videoGame = Games.GOD_OF_WAR;
	}

	@Test
	public void arrays() {
		Object[] objects = {new Object()};
		String[] strings = {"a"};
	}

	@Test
	public void generics() {
		List<Object> objectList = new ArrayList<>();
		List<String> stringList = new ArrayList<>();
	}
}
