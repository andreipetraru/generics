package gen;

import model.Game;
import org.junit.Test;

import java.lang.reflect.Method;

public class BridgeMethods {
	@Test
	public void overTheBridge() {
		for (Method m : Game.class.getMethods()) {
			if (m.getName().equals("compareTo")) {
				System.out.println(m.toGenericString());
			}
		}
	}
}
