package gen;

import java.util.List;

public class TypeParameters {
	static List<? extends Number> boundedWildCardType(List<? extends Number> list) {
		Number number = list.get(0);
		return list;
	}

	static <T extends Number> T boundedTypeParameter(T t) {
		Number number = t;
		return t;
	}
}
