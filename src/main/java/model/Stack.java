package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Stack<E> {
	private E[] elements;
	private int size = 0;
	private static final int DEFAULT_CAPACITY = 16;

	@SuppressWarnings("unchecked")
	public Stack() {
		elements = (E[]) new Object[DEFAULT_CAPACITY];
	}

	public void push(E e) {
		if (size == DEFAULT_CAPACITY) {
			throw new IllegalStateException("Maximum scalability reached");
		}
		elements[size++] = e;
	}

	public E pop() {
		if (isEmpty()) {
			throw new IllegalStateException("Cannot pop from mpty stack!");
		}
		E result = elements[--size];
		elements[size] = null;
		return result;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public void popAll(Collection<? super E> dest) {
		while (!isEmpty()) {
			dest.add(pop());
		}
	}

	public static <T extends Comparable<T>> T max (Collection<T> collection) {
		return Collections.max(collection);
	}

	public static void main(String[] args) {
		Stack<VideoGame> numberStack = new Stack<>();
		numberStack.push(Games.QUAKE);
		numberStack.push(Games.GOD_OF_WAR);

		List<Game> objects = new ArrayList<>();
		numberStack.popAll(objects);
		System.out.println(objects.size());
	}

}
