package model;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FirstPersonShooter extends VideoGame {
	public FirstPersonShooter(String name, int price) {
		super(name, price);
	}
}
