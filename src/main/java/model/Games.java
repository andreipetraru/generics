package model;

import java.util.List;

public interface Games {
	// generatia cu cheia la gat games
	Game SOTRON = new Game("Sotron", 0);

	// board games
	BoardGame CATAN = new BoardGame("Catan", 90);

	// party games
	PartyGame DIXIT = new PartyGame("Dixit", 100);
	PartyGame CODENAMES = new PartyGame("Codenames", 79);

	// video games
	VideoGame GOD_OF_WAR = new VideoGame("God of War", 250);
	VideoGame GEARS_OF_WAR = new VideoGame("Gears of War 4", 180);
	VideoGame MARIO_KART = new VideoGame("Mario Kart", 220);

	// fps
	FirstPersonShooter QUAKE = new FirstPersonShooter("Quake", 99);
	FirstPersonShooter DOOM = new FirstPersonShooter("DOOM", 180);

	List<Game> GENERIC_GAMES = List.of(DIXIT, QUAKE, MARIO_KART, SOTRON, CATAN);
	List<BoardGame> BOARD_GAMES = List.of(DIXIT, CATAN);
	List<VideoGame> VIDEO_GAMES = List.of(GOD_OF_WAR, GEARS_OF_WAR, DOOM);
	List<PartyGame> PARTY_GAMES = List.of(DIXIT, CODENAMES);
	List<FirstPersonShooter> FIRST_PERSON_SHOOTERS = List.of(DOOM, QUAKE);
	List<VideoGame> kratos = List.of(GOD_OF_WAR, GOD_OF_WAR);
}
