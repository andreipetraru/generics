package model;

import lombok.Data;

@Data
public class Game implements Comparable<Game> {
	private final String name;
	private final int price;

	public Game(String name, int price) {
		this.name = name;
		this.price = price;
	}

	@Override
	public int compareTo(Game o) {
		return this.getPrice() - o.getPrice();
	}

	@Override
	public Game clone() {
		return null;
	}
}
