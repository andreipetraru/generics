package model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class VideoGame extends Game {
	private final String rating = "R";
	public VideoGame(String name, int price) {
		super(name, price);
	}
}
