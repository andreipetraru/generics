package model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BoardGame extends Game {
	private final boolean cardGame = true;
	public BoardGame(String name, int price) {
		super(name, price);
	}
}
